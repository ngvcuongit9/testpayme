import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
// import crypto from 'crypto';
// import CryptoJS from 'crypto-js'
import { createCipheriv, randomBytes, scrypt, createDecipheriv, publicDecrypt, privateDecrypt } from 'crypto';
import { promisify } from 'util';
// import { createDecipheriv } from 'crypto';
import { map, Observable, take } from 'rxjs';
import path from 'path';
var md5 = require('md5');
var fs = require('fs');
const NodeRSA = require('node-rsa');


@Injectable()
export class AppService {
  test: any;
  constructor(private httpService: HttpService) { }
  getHello(): string {
    return 'Hello World!';
  }

  testPayMe() {
    const url = 'https://sbx-pg.payme.vn/v1/Payment/Generate';
    const Authorization = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OTExNiwiYXBwSWQiOiIwMTM1NTgwNzQwMjUiLCJhY2NvdW50SWQiOjQ0NywibWVyY2hhbnRJZCI6MTI4NTA1LCJzdG9yZUlkIjoxMDU4MTIwNywidHlwZSI6IkFQUCIsImlhdCI6MTYyNjM0NTYzN30.vk2UsfxVBfw2MkTRhvCtCz4IX2fg-fhGYIbO1NZAktw';
    const xApiClient = '013558074025';
    // const secretKey = 'MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAM8+p7xjB0lsKBUzhaGwfYeA1WY/WVA7JLYdShM4bkhxXywzW3kW1vd3PDYapuXk5vYqtpQFAqB/ZMbspShrzNcCAwEAAQ==';
    // const response = 'U2FsdGVkX1+GYujGOFPZr8MsikIYlA+wzQ89MGLA9FU28mmZ0WC5OJ9Gus8PWkkKiYSFRQEhozwfGYxCWlIuVprhz/u1WMt2hPlDb3R7C7o=';
    const publicKey = new NodeRSA('-----BEGIN PUBLIC KEY-----\n'+
    'MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAIJjFJPl6cGEcoyyFZ507ZJqF0TruAgP\n'+
    'RHq5A9ZOGGQi1nsHARJSRWjrw65Ey7jTTIWuQF1bTKxEitrxduzK6LUCAwEAAQ==\n'+
    '-----END PUBLIC KEY-----');
    const privateKey = new NodeRSA('-----BEGIN RSA PRIVATE KEY-----\n'+
    'MIICXQIBAAKBgQCn6rR0vGGQMNo5b66vrFIaYnPSsAsU+TPJDiqyEU3khBmao+t7\n'+
    'WTvlIHtECmgiZv8GRrhW2rxCFbrZlSbRBiPMhoW/UanJdJ3ntug2zAZ+MPMpq+rE\n'+
    'tUzs0dc4Za29I+rUHkmo7ytM3h8ZhvcP7IBwDeTVCXHFh2bF6Xgljprw8wIDAQAB\n'+
    'AoGBAJeP/8YGa0GM9sw1BEXIkmsCRmz7PWV28ckeImeLuvTyT3IHVvuFNczAnsJx\n'+
    'akDzGml919F9qcM0fez69YhhtFjjgCuA87n5sNSMK3yy8bKf3nnIbdWoY8jYhnwH\n'+
    'BCWhBxDP0UM4VzIqyI/7YCDHkEcub6bKikiJcdgvG9B6UnshAkEA7k7/QH7SIhAC\n'+
    'M4UnkP18/HnlKIoFB8+MwvZipyv1+ypg795LhaUB94tts2CeTOGUrIxglhr9yg1z\n'+
    'QiyqSQtvuwJBALRh7XWrMQNYcJ46JEvBcaYYwMh77R4QX2zIhzvCE3FMUBR4YwFa\n'+
    '+GFkuJxbdPCasvuHXJhy1hdoDBlzWZfTZCkCQQCeNog0U35C6wnrFn38Pq7aGDtR\n'+
    'vMA2PU8gqN86SW9Xvvz4g0b0hWZW6JR+QKkYNbO1EsWVInpXyLI6vtiXAia9AkBj\n'+
    'tdoweFAgnEuse1FC+wyalwZCDJb3Tm+hT3vJoa9jqp59okqAHkw8chPGgpuywKD9\n'+
    'f0bFEn2b9KzziJrW3Uh5AkAgAwovrMAAY2nvNPnmYBoMTQNzBLEqK9oh4U0Q1r5P\n'+
    'MLQpeYxMp6leW14aLAwoPVeo3zLidm5DXFMXkmavGZeM\n'+
    '-----END RSA PRIVATE KEY-----')
    const data = {
      amount: 100000,
      storeId: 1,
      delegateId: 2,
      partnerTransaction: '124556274354',
      desc: 'Thanh toán tiền NET – KH 0012545',
      expiryAt: '2021-04-02T01:22:36.965Z',
      ipnUrl: 'https://google.com',
      redirectUrl: 'https://google.com',
      failedUrl: 'https://www.facebook.com',
      extraData: {
        partnerTransaction: 124556274354,
        fullname: 'Nguyễn Văn A',
        billId: 10023123123
      }
    }
    const iv = randomBytes(16);
    const SecretKey = 'd0b1240e28a109e052fa34354e9915f9';

    // encrypt AES data payload
    const textApiMessage  = data + SecretKey; 
    const cipherApiMessage = createCipheriv('aes-256-ctr', SecretKey, iv);
    const encryptApiMessage = cipherApiMessage.update(textApiMessage, 'utf8', 'base64');
    console.log('encryptDataBody: ', encryptApiMessage);
    
    // encrypt RSA x-api-key
    const encryptApiKey = publicKey.encrypt(SecretKey, 'base64');
    console.log('encryptApiKey', encryptApiKey);

    // encrypt AES x-api-action
    const textApiAction = url + SecretKey;
    const cipherApiAction = createCipheriv('aes-256-ctr', SecretKey, iv);
    const encryptApiAction = cipherApiAction.update(textApiAction, 'utf8', 'base64');
    console.log('apiAction', encryptApiAction);

    // checksum Md5 x-api-validate 
    const method = 'POST';
    const apiValidate = md5(`${encryptApiAction}+${method}+${Authorization}+${encryptApiMessage}+${SecretKey}`);
    console.log('md5', apiValidate);
  
    var config = {
      headers: {
        'x-api-client': xApiClient,
        'x-api-key': encryptApiKey,
        'x-api-action': encryptApiAction,
        'x-api-validate': apiValidate,
        'Authorization': Authorization
      }
    };
    return this.httpService.post(url, encryptApiMessage, config).pipe(map(res =>
      console.log('res', res)));
    // .subscribe(res => {
    //   b = res.data;
    //   return res.data;
    // })
    // setTimeout(() => {
    //   console.log('bbbb',b);
    // }, 1000)
    // return apiPayment;
  }

  async RSADecrypt() {
    const key = new NodeRSA('-----BEGIN RSA PRIVATE KEY-----\n'+
    'MIICXQIBAAKBgQCn6rR0vGGQMNo5b66vrFIaYnPSsAsU+TPJDiqyEU3khBmao+t7\n'+
    'WTvlIHtECmgiZv8GRrhW2rxCFbrZlSbRBiPMhoW/UanJdJ3ntug2zAZ+MPMpq+rE\n'+
    'tUzs0dc4Za29I+rUHkmo7ytM3h8ZhvcP7IBwDeTVCXHFh2bF6Xgljprw8wIDAQAB\n'+
    'AoGBAJeP/8YGa0GM9sw1BEXIkmsCRmz7PWV28ckeImeLuvTyT3IHVvuFNczAnsJx\n'+
    'akDzGml919F9qcM0fez69YhhtFjjgCuA87n5sNSMK3yy8bKf3nnIbdWoY8jYhnwH\n'+
    'BCWhBxDP0UM4VzIqyI/7YCDHkEcub6bKikiJcdgvG9B6UnshAkEA7k7/QH7SIhAC\n'+
    'M4UnkP18/HnlKIoFB8+MwvZipyv1+ypg795LhaUB94tts2CeTOGUrIxglhr9yg1z\n'+
    'QiyqSQtvuwJBALRh7XWrMQNYcJ46JEvBcaYYwMh77R4QX2zIhzvCE3FMUBR4YwFa\n'+
    '+GFkuJxbdPCasvuHXJhy1hdoDBlzWZfTZCkCQQCeNog0U35C6wnrFn38Pq7aGDtR\n'+
    'vMA2PU8gqN86SW9Xvvz4g0b0hWZW6JR+QKkYNbO1EsWVInpXyLI6vtiXAia9AkBj\n'+
    'tdoweFAgnEuse1FC+wyalwZCDJb3Tm+hT3vJoa9jqp59okqAHkw8chPGgpuywKD9\n'+
    'f0bFEn2b9KzziJrW3Uh5AkAgAwovrMAAY2nvNPnmYBoMTQNzBLEqK9oh4U0Q1r5P\n'+
    'MLQpeYxMp6leW14aLAwoPVeo3zLidm5DXFMXkmavGZeM\n'+
    '-----END RSA PRIVATE KEY-----')
    // console.log('key', key);
    let data = 'U2FsdGVkX19vzlrBuvpwtq/MtORa1MNq82Q6K54ut7PNFx/tjGlJwXDXJVWi+oB+Yz+Afwb8aQbyV+qYgGwko0Jg/VCDW2EKuUhfWGbgNrs=';
    let text = 'WmalM42Rq6ol0m5/s9M+Jy0m5lEbBRWv6JZGa1KgT9/QdNCuxo27Et+B0/SMXQbLmhbkfTRivHc9Q54Nzpr10LnAWcx9ulfgifdbsltgSctHQKFBggy7ZTtIUWuTshU32uOEylhJ9k0Z6M3eImmQY3aIenaXFy2N+f7OVifnh7A=';
    const SecretKey = key.decrypt(text,  'utf8');
    console.log('SecretKey: ', SecretKey);
    const iv = randomBytes(16); 
    let messageToDecrypt = Buffer.from(data, "base64");
    const decipher = createDecipheriv('aes-256-cbc', SecretKey, iv);
    var decipherBody = decipher.update(messageToDecrypt);
    console.log('data body', decipherBody);

  }
  



}
