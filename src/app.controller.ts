import { Controller, Get, Post } from '@nestjs/common';
import { map, Observable } from 'rxjs';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello() {
    return this.appService.RSADecrypt();
  }
 
  
  @Post()
  testPayme()  {
      const a = this.appService.testPayMe();
    //  console.log('aaa', a);
      return a;
  }

  
}
